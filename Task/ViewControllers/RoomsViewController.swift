//
//  RoomsViewController.swift
//  Task
//
//  Created by apple on 25/03/22.
//

import UIKit

class RoomsViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var roomsTableView:UITableView!
    
    
    var rooms = [Room]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "People"
        registerCell()
        getRoomsData()
    }
    
    
    // Here we are registering tableview cell Nib 
    func registerCell(){
        self.roomsTableView.register(UINib(nibName: "RoomsTableViewCell", bundle: nil), forCellReuseIdentifier: "RoomsTableViewCell")
    }
    
    /// This function is used to get data from server and populate in tableview. This will use a method which written in singleton class. we will be passing endpoint only, all the network call related code written there and will give us response here.
    func getRoomsData(){
        
        ServiceManager.shared.getRequest(endPoint: EndPoints.rooms) { result in
            switch result{
            case .success(let data):
                print("Rooms APi Response =====>>>>>> ",data)
                if data.count != 0{
                    for item in data{
                        let room = Room(json: item)
                        self.rooms.append(room)
                    }
                    DispatchQueue.main.async {
                        self.roomsTableView.reloadData()
                    }
                }else{
                    self.showAlert(title: "Oops", message: "No Data Found!") {}
                }
                break
            case .failure(let error):
                print("Rooms APi error ===>>>",error.label)
                self.showAlert(title: "Oops", message: error.label) {}
                break
            }
        }
    }

}

extension RoomsViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rooms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RoomsTableViewCell") as! RoomsTableViewCell
        cell.configureRoomData(room: rooms[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}
