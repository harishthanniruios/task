//
//  ViewController.swift
//  Task
//
//  Created by apple on 25/03/22.
//

import UIKit

class ViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var peopleTableView:UITableView!
    
    
    //MARK:- Properties
    var people = [People]()
    
    
    
    //MARK:- View Lift Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        registerTableViewCellNib()
        getPeopleData()
        self.title = "People"
    }
    
    
    
    // Here we are registering tableview cell Nib
    func registerTableViewCellNib(){
        self.peopleTableView.register(UINib(nibName: "PeopleTableViewCell", bundle: nil), forCellReuseIdentifier: "PeopleTableViewCell")
    }
    
    
    
    /// This function is used to get data from server and populate in tableview. This will use a method which written in singleton class. we will be passing endpoint only, all the network call related code written there and will give us response here.
    func getPeopleData(){
        
        ServiceManager.shared.getRequest(endPoint: EndPoints.people) { result in
            switch result{
            case .success(let data):
                print("people APi Response =====>>>>>> ",data)
                if data.count != 0{
                    for item in data{
                        let person = People(json: item)
                        self.people.append(person)
                        
                    }
                    DispatchQueue.main.async {
                        self.peopleTableView.reloadData()
                    }
                }else{
                    
                    self.showAlert(title: "Oops", message: "No Data Found!") {}
                }
                break
            case .failure(let error):
                print("people APi error ===>>>",error.label)
                self.showAlert(title: "Oops", message: error.label) {}
                break
            }
        }
    }
    
}

//MARK:- TableView Delegate Methods.
extension ViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return people.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PeopleTableViewCell") as! PeopleTableViewCell
        cell.configurePersonData(person: people[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PersonDetailsViewController" ) as! PersonDetailsViewController
        vc.personData = people[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}
