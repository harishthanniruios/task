//
//  PersonDetailsViewController.swift
//  Task
//
//  Created by apple on 25/03/22.
//

import UIKit

class PersonDetailsViewController: UIViewController {

    //MARK:- IBOutlets
    
    @IBOutlet weak var personImgView:UIImageView!
    @IBOutlet weak var personEmailLbl:UILabel!
    @IBOutlet weak var personJobTitleLbl:UILabel!
    @IBOutlet weak var personNameLbl:UILabel!
    @IBOutlet weak var personFavorateColorLbl:UILabel!
    @IBOutlet weak var createDateLbl:UILabel!
    
    //MARK:- Properties
    var personData:People?
    
    
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configurePersonData()
    }
    
     //MARK:- Updating Person UI
    func configurePersonData(){
        if let obj = self.personData{
            
            personNameLbl.text = obj.firstName + obj.lastName
            personEmailLbl.text = obj.email
            personJobTitleLbl.text = obj.jobtitle
            personFavorateColorLbl.text = obj.favouriteColor
            createDateLbl.text = obj.createdAt.displayDate(withFormat: .preDefined, displayFormat: .webservice, timeZoneRequired: false)
            
            personImgView.clipsToBounds = true
            personImgView.layer.cornerRadius = personImgView.frame.size.width / 2
            UIImage.loadFrom(url: URL(string: obj.avatar)!) { image in
                if image != nil{
                    self.personImgView.image = image
                }else{
                    self.personImgView.image = UIImage(named: "profile_default")
                }
                
            }
        }
    }
   

}
