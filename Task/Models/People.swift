//
//  People.swift
//  Task
//
//  Created by apple on 25/03/22.
//

import Foundation



struct People {
    var id :Int = 0
    var lastName = ""
    var email = ""
    var favouriteColor = ""
    var avatar = ""
    var jobtitle = ""
    var createdAt = ""
    var firstName = ""
    
    
    
    init(json:[String:Any]) {
        if let id = json["id"] as? Int{
            self.id = id
        }
        if let lastName = json["lastName"] as? String{
            self.lastName = lastName
        }
        if let email = json["email"] as? String{
            self.email = email
        }
        if let favouriteColor = json["favouriteColor"] as? String{
            self.favouriteColor = favouriteColor
        }
        if let avatar = json["avatar"] as? String{
            self.avatar = avatar
        }
        if let jobtitle = json["jobtitle"] as? String{
            self.jobtitle = jobtitle
        }
        if let createdAt = json["createdAt"] as? String{
            self.createdAt = createdAt
        }
        if let firstName = json["firstName"] as? String{
            self.firstName = firstName
        }
    }
    
}




//["id": 2,
//"lastName": Weber,
//"email": Milton.Wisoky@gmail.com,
//"favouriteColor": sky blue,
//"avatar": https://randomuser.me/api/portraits/women/23.jpg,
//"jobtitle": Principal Accounts Developer,
//"createdAt": 2022-01-24T22:47:43.227Z,
//"firstName": Armando]
