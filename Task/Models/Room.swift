//
//  Room.swift
//  Task
//
//  Created by apple on 25/03/22.
//

import Foundation
import UIKit


struct Room {
    var createdAt = ""
    var isOccupied:Bool = false
    var id :Int = 0
    var maxOccupancy:Int = 0
    
    
    init(json:[String:Any]) {
        if let id = json["id"] as? Int{
            self.id = id
        }
        if let createdAt = json["createdAt"] as? String{
            self.createdAt = createdAt
        }
        if let isOccupied = json["isOccupied"] as? Bool {
            self.isOccupied = isOccupied
        }
        if let maxOccupancy = json["maxOccupancy"] as? Int{
            self.maxOccupancy = maxOccupancy
        }
        
    }
}

//["createdAt": 2022-01-24T20:52:50.765Z, "isOccupied": 0, "id": 1, "maxOccupancy": 53539]

