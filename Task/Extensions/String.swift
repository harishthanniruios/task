//
//  String.swift
//  Task
//
//  Created by apple on 25/03/22.
//

import Foundation
import UIKit


extension String{
    func displayDate(withFormat: DateFormat = .preDefined, displayFormat: DateFormat = .webservice, timeZoneRequired: Bool = true) -> String? {
        let formattor = HKDateFormator.format(dateFormat: withFormat)
        formattor.timeZone = TimeZone(abbreviation: "UTC")
        if let date = formattor.date(from: self) {
            formattor.dateFormat = displayFormat.format
            if timeZoneRequired {
                formattor.timeZone = TimeZone.autoupdatingCurrent
            }
            return formattor.string(from: date)
        } else {
            return self
        }
    }
}
