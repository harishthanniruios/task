//
//  UIViewController.swift
//  Task
//
//  Created by apple on 25/03/22.
//

import Foundation
import UIKit


extension UIViewController{
    
    func showAlert(title: String, message: String, callback: @escaping () -> ()) {
        DispatchQueue.main.async {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
            alertAction in
            callback()
        }))
        
        self.present(alert, animated: true, completion: nil)
        }
    }
}
