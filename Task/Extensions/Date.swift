//
//  Date.swift
//  Task
//
//  Created by apple on 25/03/22.
//

import Foundation
import UIKit

enum DateFormat {
    case preDefined
    case webservice
    
    var format: String {
        switch self {
        case .preDefined: return "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        case .webservice: return "dd-MM-yyyy"
        }
    }
}
class HKDateFormator: NSObject {
    class func format(dateFormat: DateFormat = .preDefined) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat.format
        dateFormatter.timeZone = .current
        return dateFormatter
    }
}
