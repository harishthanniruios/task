//
//  ServiceManager.swift
//  Task
//
//  Created by apple on 25/03/22.
//

import Foundation
import UIKit

enum ApiError: Error {
    case badURL
    case internetError
    case timeOut
    case technicalError
    case parsingError
    case authentication
    
    var label: String {
        switch self {
        case .badURL:
            return "Not a valid URL"
        case .internetError:
            return "Slow or no internet connection. Please check your internet settings."
        case .timeOut:
            return "request time out please try again."
        case .technicalError:
            return "Something Went Wrong! Our system may be having some trouble, Please try again later. We apologize for the inconvenience."
        case .parsingError:
            return "No data recivied from server."
        case .authentication:
            return ""
        }
    }
}


class ServiceManager {
    static var shared = ServiceManager()
    typealias ResponseCode = Int
    typealias tuple = (Int,Error?,[String:Any]?)
    func getRequest(endPoint: String,completionHandler: @escaping (Result<[[String:Any]], ApiError>) -> ()){

        let urlString = "\(EndPoints.baseUrl)" + endPoint
        let url = URL(string: urlString)
        print("api URL ===>>> ",urlString)
        var request = URLRequest(url: url!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 40.0)
        request.httpMethod = HTTPMethodType.GET
        request.setValue(HTTPHeadersValues.APPLICATION_JSON, forHTTPHeaderField: HTTPheaders.Content_Type)
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if error == nil{
                guard let httpResponse = response as? HTTPURLResponse else {
                    completionHandler(.failure(.technicalError))
                    return
                }
                if httpResponse.statusCode == 401{
                    completionHandler(.failure(.authentication))
                    return
                }
                if let info = data {
                    do {
                        let jsonResponse = try JSONSerialization.jsonObject(with: info, options: JSONSerialization.ReadingOptions(rawValue: 0)) as? [[String:Any]]
                        //print("json Response ===>> ",jsonResponse)
                        completionHandler(.success(jsonResponse!))
                    } catch let error as NSError {
                        print("error",error.localizedDescription)
                        completionHandler(.failure(.parsingError))
                    }
                }
                else{
                    completionHandler(.failure(.technicalError))
                }
            }else{
                completionHandler(.failure(.timeOut))
            }
        }
        dataTask.resume()
        
    }
    
    
}

extension Data {
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}
enum HTTPMethodType {
    static let POST = "POST"
    static let GET = "GET"
    static let PUT = "PUT"

}
enum HTTPHeadersValues {
    static let APPLICATION_JSON = "application/json"
}


enum HTTPheaders{
    static let Content_Type = "Content-Type"
    static let Authorization = "Authorization"
}
