//
//  RoomsTableViewCell.swift
//  Task
//
//  Created by apple on 25/03/22.
//

import UIKit

class RoomsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var creationDateLbl:UILabel!
    @IBOutlet weak var maxOccupancyLbl:UILabel!
    @IBOutlet weak var currentStatusLbl:UILabel!
    @IBOutlet weak var backView:UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureRoomData(room:Room){
        
        //configure BackView
        backView.layer.cornerRadius = 8
        backView.layer.shadowColor = UIColor(displayP3Red: 154/255, green: 154/255, blue: 154/255, alpha: 1).cgColor
        backView.layer.shadowOpacity = 1
        backView.layer.shadowOffset = CGSize(width: 1, height: 1)
        backView.layer.shadowRadius = 5
        
        // configure Rooms data
        
        creationDateLbl.text = room.createdAt.displayDate(withFormat: .preDefined, displayFormat: .webservice, timeZoneRequired: false)
        maxOccupancyLbl.text = "\(room.maxOccupancy)"
        if room.isOccupied{
            currentStatusLbl.text = "Occupaid"
            currentStatusLbl.textColor = .systemRed
        }else{
            currentStatusLbl.text = "Available"
            currentStatusLbl.textColor = .systemGreen
        }
    }
    
    
}
