//
//  PeopleTableViewCell.swift
//  Task
//
//  Created by apple on 25/03/22.
//

import UIKit

class PeopleTableViewCell: UITableViewCell {

    
    @IBOutlet weak var backView:UIView!
    @IBOutlet weak var userImgView:UIImageView!
    @IBOutlet weak var userNameLbl:UILabel!
    @IBOutlet weak var userEmailLbl:UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configurePersonData(person:People){
        //configure BackView
        backView.layer.cornerRadius = 8
        backView.layer.shadowColor = UIColor(displayP3Red: 154/255, green: 154/255, blue: 154/255, alpha: 1).cgColor
        backView.layer.shadowOpacity = 1
        backView.layer.shadowOffset = CGSize(width: 1, height: 1)
        backView.layer.shadowRadius = 5
        
        // configure User data
        userImgView.clipsToBounds = true
        userImgView.layer.cornerRadius = userImgView.frame.size.width / 2
        userNameLbl.text = person.firstName + person.lastName
        userEmailLbl.text = person.email
        if person.avatar != ""{
            UIImage.loadFrom(url: URL(string: person.avatar)!) { image in
                if image != nil{
                    self.userImgView.image = image
                }else{
                    self.userImgView.image = UIImage(named: "profile_default")
                }
                
            }
        }
        
    }
    
}
